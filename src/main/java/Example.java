import com.google.cloud.functions.BackgroundFunction;
import com.google.cloud.functions.Context;
import com.google.cloud.kms.v1.AsymmetricDecryptResponse;
import com.google.cloud.kms.v1.CryptoKeyVersionName;
import com.google.cloud.kms.v1.KeyManagementServiceClient;
import com.google.cloud.storage.*;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;

public class Example implements BackgroundFunction<Example.PubSubMessage> {

	private static final Logger logger = Logger.getLogger(Example.class.getName());

	private static String PROJECT_ID;

	private static String KEY_LOCATION;

	private static String KEY_RING;

	private static String KEY;

	private static String KEY_VERSION;

	private static String DECRYPTED_FOLDER;

	static {
		PROJECT_ID = Optional.ofNullable(System.getenv("PROJECT_ID")).orElseThrow(() -> new RuntimeException("can't find env 'PROJECT_ID'"));
		KEY_LOCATION = Optional.ofNullable(System.getenv("KEY_LOCATION")).orElseThrow(() -> new RuntimeException("can't find env 'KEY_LOCATION'"));
		KEY_RING = Optional.ofNullable(System.getenv("KEY_RING")).orElseThrow(() -> new RuntimeException("can't find env 'KEY_RING'"));
		KEY = Optional.ofNullable(System.getenv("KEY")).orElseThrow(() -> new RuntimeException("can't find env 'KEY'"));
		KEY_VERSION = Optional.ofNullable(System.getenv("KEY_VERSION")).orElseThrow(() -> new RuntimeException("can't find env 'KEY_VERSION'"));
		DECRYPTED_FOLDER = Optional.ofNullable(System.getenv("DECRYPTED_FOLDER")).orElseThrow(() -> new RuntimeException("can't find env 'DECRYPTED_FOLDER'"));
	}

	@Override
	public void accept(PubSubMessage message, Context context) throws IOException {
		final String bucketId = message.attributes.bucketId;
		final String objectId = message.attributes.objectId;
		Storage storage = StorageOptions.newBuilder().setProjectId(PROJECT_ID).build().getService();
		logger.info("received bucketId : " + bucketId + ", objectId : " + objectId);
		Blob blob = storage.get(BlobId.of(bucketId, objectId));
		final byte[] content = blob.getContent();

		try (KeyManagementServiceClient client = KeyManagementServiceClient.create()) {
			// Build the key version name from the project, location, key ring, key, and key version.
			CryptoKeyVersionName keyVersionName =
							CryptoKeyVersionName.of(PROJECT_ID, KEY_LOCATION, KEY_RING, KEY, KEY_VERSION);

			// Decrypt the ciphertext.
			AsymmetricDecryptResponse response =
							client.asymmetricDecrypt(keyVersionName, ByteString.copyFrom(content));
			logger.info("Plaintext: " + response.getPlaintext().toStringUtf8());
			final String decryptedFileName = objectId.split("/")[1]
							.replace("-encrypted", "");
			// Upload Plaintext to Cloud Storage
			BlobId blobId = BlobId.of(bucketId, DECRYPTED_FOLDER + decryptedFileName);
			BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
			storage.create(blobInfo, response.getPlaintext().toByteArray());
		}
	}

	public static class PubSubMessage {
		Attributes attributes;

		public static class Attributes {
			String bucketId;
			String objectId;
		}
	}
}
